<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Smalot\PdfParser\Parser;

class PdfController extends Controller
{


    public function search()
    {
        $filePath = public_path('test.txt');

        $text = file_get_contents($filePath);
        $text = str_replace(["\n", "\f"], " ", $text);
        $text = str_replace([".", ",", ";", ":", "!", "?", "-", "(", ")", "[", "]", "{", "}", "'", "\""], ' ', $text);
        $text = str_replace('  ', ' ', $text);



        function countWordOccurrences($text) {
            // Convert the text to lowercase to ensure case-insensitive counting
            $text = strtolower($text);

            // Remove punctuation using str_replace
            $text = str_replace(
                [".", ",", ";", ":", "!", "?", "-", "(", ")", "[", "]", "{", "}", "'", "\""],
                "",
                $text
            );

            // Split the text into words
            $words = explode(' ', $text);
//            dd(count($words));
            // Initialize an associative array to hold the word counts
            $wordCounts = [];

            // Iterate through each word
            foreach ($words as $word) {
                // Trim any remaining whitespace
                $word = trim($word);

                // Skip empty words (which can occur due to multiple spaces)
                if ($word === '') {
                    continue;
                }

                // If the word is already in the array, increment its count
                if (isset($wordCounts[$word])) {
                    $wordCounts[$word]++;
                } else {
                    $wordCounts[$word] = 1;
                }
            }
            return $wordCounts;
        }
//        $massiv = [];
        $wordCounts = countWordOccurrences($text);
        foreach ($wordCounts as $word => $count) {
            dd("The word '$word' occurs $count times.");
        }
//        dd($wordCounts['antoine']);

    }

//    public function search(Request $request)
//    {
//        $pdfFilePath = public_path('test.pdf');
//        $parser = new Parser();
//        $pdf = $parser->parseFile($pdfFilePath);
//        $text = $pdf->getText();
//        $wordsArray = explode(' ', $text);
//        $first5000WordsArray = array_slice($wordsArray, 0, 5000);
//        $first5000WordsText = implode(' ', $first5000WordsArray);
//
//        dd($first5000WordsText);
//
//        function countWordOccurrences($text) {
//            // Convert the text to lowercase to ensure case-insensitive counting
//            $text = strtolower($text);
//
//            // Remove punctuation using str_replace
//            $text = str_replace(
//                [".", ",", ";", ":", "!", "?", "-", "(", ")", "[", "]", "{", "}", "'", "\""],
//                "",
//                $text
//            );
//
//            // Split the text into words
//            $words = explode(' ', $text);
//
//            // Initialize an associative array to hold the word counts
//            $wordCounts = [];
//
//            // Iterate through each word
//            foreach ($words as $word) {
//                // Trim any remaining whitespace
//                $word = trim($word);
//
//                // Skip empty words (which can occur due to multiple spaces)
//                if ($word === '') {
//                    continue;
//                }
//
//                // If the word is already in the array, increment its count
//                if (isset($wordCounts[$word])) {
//                    $wordCounts[$word]++;
//                } else {
//                    $wordCounts[$word] = 1;
//                }
//            }
//            return $wordCounts;
//        }
//
//
//
//// Get the word counts
//        $wordCounts = countWordOccurrences($text);
//        dd($wordCounts);
////        foreach ($wordCounts as $word => $count) {
////            echo "The word '$word' occurs $count times.\n";
////        }
//
//
//    }
}
