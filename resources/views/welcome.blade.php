<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        body {
            background-image: linear-gradient(to right top,#D91B23,#124FEB);
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center
        }

        .search {
            background-color: #fff;
            padding: 4px;
            border-radius: 5px
        }

        .search-1 {
            position: relative;
            width: 100%
        }

        .search-1 input {
            height: 45px;
            border: none;
            width: 100%;
            padding-left: 34px;
            padding-right: 10px;
            border-right: 2px solid #eee
        }

        .search-1 input:focus {
            border-color: none;
            box-shadow: none;
            outline: none
        }

        .search-1 i {
            position: absolute;
            top: 12px;
            left: 5px;
            font-size: 24px;
            color: #eee
        }

        ::placeholder {
            color: #eee;
            opacity: 1
        }

        .search-2 {
            position: relative;
            width: 100%
        }

        .search-2 input {
            height: 45px;
            border: none;
            width: 100%;
            padding-left: 18px;
            padding-right: 100px
        }

        .search-2 input:focus {
            border-color: none;
            box-shadow: none;
            outline: none
        }

        .search-2 i {
            position: absolute;
            top: 12px;
            left: -10px;
            font-size: 24px;
            color: #eee
        }

        .search-2 button {
            position: absolute;
            right: 1px;
            top: 0px;
            border: none;
            height: 45px;
            background-color: red;
            color: #fff;
            width: 90px;
            border-radius: 4px
        }

        @media (max-width:800px) {
            .search-1 input {
                border-right: none;
                border-bottom: 1px solid #eee
            }

            .search-2 i {
                left: 4px
            }

            .search-2 input {
                padding-left: 34px
            }

            .search-2 button {
                height: 37px;
                top: 5px
            }
        }
    </style>
</head>
<body>
<div class="container">
        <div class="search">
            <div class="row">
                <form action="{{route("search")}}" method="POST">
                    @csrf
                    <div class="col-md-12">
                        <div>
                            <div class="search-2">
                                <input name="search" type="text" placeholder="San Francisco,USA">
                                <button type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>
</body>
</html>
